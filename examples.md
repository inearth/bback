## Api requests: ##
To authorize via social network:

__Instagram__

1. Direct your user to the https://api.instagram.com/oauth/authorize/?client_id=CLIENT-ID&redirect_uri=REDIRECT-URI&response_type=token in browser, where CLIENT-ID - is taken from the instagram dev client.

2. Send POST Request to create user in burb API:

```bash
curl -X POST -d "client_id=6dTvS6JKipsDgd9uIOOJjk6uO3JvOtolNTCgdxkh&client_secret=lgoocnXvWL6aEvxZ7FSpwagyvqzwo87GaDnJhixSFgSnFwEeYi8t5Dah5HaTfACZYEN6Jkjk3tjrgWzooT4yTjpQvESSArnnsMjDjyEWz8bFPqZe7vqlJkEIlWLPrgu7&grant_type=convert_token&backend=instagram&token=<TOKEN_FROM_INSTAGRAM>" http://burber.pythonanywhere.com/auth/convert-token
```

where `client_id` and `client_secret` are taken from the created 'Django OAuth Toolkit' app (for example, in admin page: Home › Django OAuth Toolkit › Applications › Burb_oauth)

## Pythonanywhere test server: ##

### To update: ###
1. Login to https://pythonanywhere.com (in.earth@ya.ru / xxXX1234)
2. Click on 'Consoles' -> "Other:    Bash" (or if it's already open - choose that one)
3. In console: 
    `workon venv`
    `cd /home/burber/bback`
    `git pull`
    `python manage.py migrate`
4. Click on "Web" link and then press the green button "Reload burber.pythonanywhere.com"

