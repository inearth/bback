#!/usr/bin/env bash

python manage.py makemigrations burbuser
python manage.py makemigrations otp
python manage.py migrate
python manage.py runserver 0.0.0.0:8000
