from django.utils.timezone import now

from .models import User


class SetLastActiveMiddleware:

    """Additional logic to the User model 'last_active' field."""

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        return self.get_response(request)

    def process_response(self, request, response):
        """
        The 'last_active' field will be updated on every request.

        :param request: Standard django request object.
        :param response: Standard django response object.

        :returns: Django response object.
        """
        if request.user.is_authenticated():
            # Update last visit time after request finished processing.
            User.objects.filter(pk=request.user.pk).update(last_visit=now())
        return response
