import datetime

from django.db import models
from django.core.validators import RegexValidator
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import BaseUserManager


class MyUserManager(BaseUserManager):

    """
    A custom user manager to deal with phones as unique identifiers for auth
    instead of usernames. The default that's used is "UserManager".
    """

    def _create_user(self, phone_number, password, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        :param str phone_number: User's phone number.
        :param str password: User's password.
        :param dict extra_fields: Any key-value data related to creating users.
        """
        if not phone_number:
            raise ValueError('The Phone must be set')
        user = self.model(phone_number=phone_number, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, phone_number, password, **extra_fields):
        """
        Overriden django's method to create superusers.
        :param str phone_number: User's phone number.
        :param str password: User's password.
        :param dict extra_fields: Any key-value data related to creating users.
        """
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')
        return self._create_user(phone_number, password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):

    """Custom User model."""

    _phone_regex = RegexValidator(
        regex=r'^\+?1?\d{9,15}$',
        message="Allowed phone format: '+01234567890'. Up to 15 digits allowed.")
    phone_number = models.CharField(
        validators=[_phone_regex], max_length=15, blank=False, unique=True)

    email = models.EmailField(blank=True)
    first_name = models.CharField(max_length=30, blank=True)
    last_name = models.CharField(max_length=30, blank=True)
    date_joined = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_verified = models.BooleanField(default=False)
    is_barber = models.BooleanField(default=False)
    birthday = models.DateField(blank=False, null=True)
    auth_method = models.CharField(max_length=100, blank=True, choices=(
        ('vk', 'vk.com'), ('instagramm', 'instagramm.com')))
    last_active = models.DateTimeField(auto_now=True)
    image = models.ImageField(
        upload_to="img/profiles", default='img/profiles/default_user.png', blank=True)

    def age(self):
        """
        Attribute to get current user age.

        :returns: Integer of users' age.
        """
        if self.birthday:
            return int((datetime.date.today() - self.birthday).days / 365.25)

    latitude = models.DecimalField(max_digits=9, decimal_places=6, null=True)
    longitude = models.DecimalField(max_digits=9, decimal_places=6, null=True)

    USERNAME_FIELD = 'phone_number'

    objects = MyUserManager()

    class Meta:
        verbose_name = 'user'
        verbose_name_plural = 'users'
