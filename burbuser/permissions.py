from rest_framework import permissions


class IsAuthenticatedOrCreate(permissions.IsAuthenticated):

    """Checks that user is authenticated, and if so - that request is 'POST'."""

    def has_permission(self, request, view):
        """
        Method-visitor for checking permissions.

        :param request: Standard django request object.
        :param view: A caller view, that uses this permission class.

        :returns: Boolean True if user is authenticated and makes a 'POST' request.
        """
        if request.method == 'POST':
            return True
        return super(IsAuthenticatedOrCreate, self).has_permission(request, view)
