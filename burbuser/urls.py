from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from rest_framework.routers import DefaultRouter


from burbuser import views

router = DefaultRouter()
router.register(r'users', views.UserViewSet)


# The API URLs are now determined automatically by the router.
# Additionally, we include the login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
    path('social_auth/', include('rest_framework_social_oauth2.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
