"""Main DRF serializers for the 'Burb' app."""

from rest_framework import serializers
from burbuser.models import User


class UserSerializer(serializers.HyperlinkedModelSerializer):

    """Represents users. Extends standard user with custom fields."""

    class Meta:

        """User meta serializer data."""

        model = User
        fields = (
            'id',
            'first_name',
            'last_name',
            'birthday',
            'email',
            'phone_number',
            'is_verified',
            'is_barber',
            'age',
            'auth_method',
            'last_active',
            'image',
            'latitude',
            'longitude'
        )
