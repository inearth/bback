from rest_framework import viewsets

from burbuser.permissions import IsAuthenticatedOrCreate
from burbuser.serializers import UserSerializer
from burbuser.models import User


class UserViewSet(viewsets.ModelViewSet):

    """This viewset automatically provides `list` and `detail` actions."""

    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get_object(self):
        """
        Method for getting currently logged in user object on '/me/' request.

        :returns: Current user object.
        """
        pk = self.kwargs.get('pk')
        if pk == "me":
            return self.request.user
        return super().get_object()
