from django.apps import AppConfig


class BurbuserConfig(AppConfig):
    name = 'burbuser'
