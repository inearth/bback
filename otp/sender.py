"""Module implements the sending of a message to sms center."""
import re

import requests

_SMS_CENTER_URL = 'http://smsc.ru/sys/send.php'
_LOGIN = 'serebryakov'
_PASSWORD = 'EwbAveulbyitoa9'
_CHARSET = 'utf-8'
_MESSAGE_TEMPLATE = 'BURB. Код доступа:'
_SENDER = 'BURB'


class PasscodeSender:

    """Class implements the sending of a message to sms center."""

    def __init__(self, phone_number):
        """
        :param str phone_number: String with the phone number.

        :raises AssertionError: If value with the phone number is wrong.
        """
        assert re.match(r'^\+?1?\d{9,15}$', phone_number), (
            'Allowed phone number format: "+01234567890". Up to 15 digits allowed.')
        self.__login = _LOGIN
        self.__password = _PASSWORD
        self.__charset = _CHARSET
        self.__message = _MESSAGE_TEMPLATE
        self.__sender = _SENDER
        self.__phone_number = phone_number
        self.__passcode = None

    @property
    def phone_number(self):
        """
        Get the phone number that will receive the message.

        :returns: The phone number.
        """
        return self.__phone_number

    @phone_number.setter
    def phone_number(self, value):
        """
        Set the phone number that will receive the message.

        :param str value: String with the phone number.

        :raises AssertionError: If value with the phone number is wrong.
        """
        assert re.match(r'^\+?1?\d{9,15}$', value), (
            'Allowed phone number format: "+01234567890". Up to 15 digits allowed.')
        self.__phone_number = value

    @property
    def passcode(self):
        """
        Get the passcode to be sent to the subscriber.

        :returns: The passcode.
        """
        assert self.__passcode, 'The passcode is empty. It must be set.'
        return self.__passcode

    @passcode.setter
    def passcode(self, value):
        """
        Set the passcode to be sent to the subscriber.

        :param str value: String with the passcode.
        """
        self.__passcode = value

    def _prepare_data(self):
        """
        Prepare some parameters for the POST request.

        :returns: Dictionary with some parameters.
        """
        return {
            'login': self.__login,
            'psw': self.__password,
            'charset': self.__charset,
            'phones': self.phone_number,
            'mes': f'{_MESSAGE_TEMPLATE} {self.passcode}',
            'sender': self.__sender
        }

    def send_message(self):
        """
        Send a sms to the sms center.

        :returns: Response object with some response params.
        """
        return requests.post(_SMS_CENTER_URL, data=self._prepare_data())
