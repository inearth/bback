from django.urls import path, re_path
from rest_framework.routers import DefaultRouter

from . import views


# router = DefaultRouter()
# router.register(r'generate', views.TOTPCreateView, base_name='otp-create')


urlpatterns = [
    path('create', views.OTPGenerateViewSet.as_view(), name='totp-create'),
    path('verify', views.OTPVerifyViewSet.as_view(), name='totp-login'),
]
