from rest_framework import views
from rest_framework.response import Response
from rest_framework import status
import pyotp
from rest_framework.authtoken.models import Token
from django.contrib.auth import get_user_model


class OTPGenerateViewSet(views.APIView):

    """This view can only create one-time passwords."""

    def post(self, request):
        """
        Generate time-based one-time password.

        :param request: DRF 'request' object.
        :returns: DRF 'Response' object with otp code.
        """
        totp = pyotp.TOTP('base32secret3232', interval=300)
        code = totp.now()
        # TODO: send via sms (e.g. implement send_sms(totp))
        return Response(code, status=status.HTTP_201_CREATED)


class OTPVerifyViewSet(views.APIView):

    """This view can only create one-time passwords."""

    def post(self, request):
        """
        Verify one-time passwords.

        :param request: DRF 'request' object.
        :returns: DRF 'Response' object with token or only status with HTTP error.
        """
        data = request.data
        user_id, code = data['user_id'], data['code']
        user = get_user_model().objects.get(pk=user_id)
        if user.is_verified:
            return Response(status=status.HTTP_409_CONFLICT)
        totp = pyotp.TOTP('base32secret3232', interval=300)
        if totp.verify(code):
            user.is_verified = True
            user.save(update_fields=["is_verified"])
            token = Token.objects.create(user=user)
            return Response(token.key, status=status.HTTP_201_CREATED)
        return Response(status=status.HTTP_400_BAD_REQUEST)
