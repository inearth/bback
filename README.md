# README #

Backend for the "Burb" project. Based on Python.

## Local development ##
*Development using docker. Below commands will start interactive mode in container and mount 'bback' folder to the container's '/burb' dir. If needed look up other options in the internet.*

1. Install docker
2. `cd bback`
3. `docker build -t burb .`  # See the dot at the end!
4. * If you already made a migrations:
    
    `docker run -p 8000:8000 --rm -it -v ${PWD}:/burb burb`  # This will run container with django dev server
    
    * If you did not make a migrations:
    
    `docker run -p 8000:8000 --rm -it -v ${PWD}:/burb burb bash` # Enters in interactive mode

    `python manage.py migrate`  # This will create a db.sqlite
    `python manage.py createsuperuser`  # Creds are entered interactively

    `python manage.py runserver 0.0.0.0:8000`

5. Open in browser http://localhost:8000
