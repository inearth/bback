FROM python:3.6

EXPOSE 8000

WORKDIR /burb
ADD requirements.txt /burb
RUN pip install -r requirements.txt --trusted-host pypi.python.org

# TODO: Not necessary for now, but will be used soon
ENV ENVIRONMENT=dev

CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
